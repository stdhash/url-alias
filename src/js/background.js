'use strict';

var chrome = window.chrome;
var items = {};

function load() {
	chrome.storage.sync.get('urlalias', function(response) {
		items = response.urlalias;
	});
}

function redirect(id, server, extra) {
	var link = items[server];
	extra = extra;

	if (!link) {
		for (var i in items) {
			if (i.startsWith(server)) {
				link = items[i];
				break;
			}
		}
	}

	if (!link) {
		return;
	}

	if (link.indexOf('://') === -1) {
		link = 'https://' + link;
	}

	chrome.tabs.update(id, {url: link});
}

function update(id, info, tab) {
	var url = tab.url;
	var stripped = /^http[s]?:\/\/(.*)/g.exec(url);

	if (!!stripped && stripped.length > 1) {
		var link = stripped[1].split('/');
		return redirect(id, link[0], link.splice(1));
	}
}

chrome.tabs.onUpdated.addListener(update);

load();
