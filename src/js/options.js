'use strict';

var chrome = window.chrome;
var $ = window.jQuery;
var elmId = 'items';
var saveId = 'save';
var addId = 'addItem';

function load() {
	chrome.storage.sync.get('urlalias', function(items) {
		items = items.urlalias;

		for (var i in items) {
			if (items.hasOwnProperty(i)) {
				addItem(i, items[i]);
			}
		}

		$('#' + saveId).click(save);
		$('#' + addId).click(function() {
			addItem();
		});
	});
}

function save() {
	var list = {};

	var items = $('#' + elmId + ' input');

	var i = 0;
	var iLen = items.length;

	for (; i < iLen; i += 2) {
		list[items[i].value] = items[i + 1].value;
	}

	chrome.storage.sync.set({urlalias: list});
}

function addItem(alias, link) {
	var row = $('<tr/>');

	var remove = $('<button/>').text('Delete');
	remove.on('click', function() {
		row.remove();
	});

	row.append($('<td/>').append($('<input/>').val(alias)))
		.append($('<td/>').append($('<input/>').val(link)))
		.append($('<td/>').append(remove));

	$('#' + elmId).append(row);
}

$(document).ready(load);
